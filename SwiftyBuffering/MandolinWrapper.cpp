//
//  MandoCPP.cpp
//  SwiftyBuffering
//
//  Created by Ariel Elkin on 06/04/2015.
//  Copyright (c) 2015 Ariel. All rights reserved.
//

#include <CoreFoundation/CoreFoundation.h>
#include "MandolinWrapper.h"
#include "Mandolin.h"

stk::Mandolin *myMandolin;

//The mandolin class makes use of wave files,
//this tells the STK library where they are:
void setRawWavePath() {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef url = CFBundleCopyResourceURL(mainBundle, CFSTR("rawwaves"), CFSTR("bundle"), NULL);
    
    if (url) {

        CFStringRef rawwavesPathCFString = CFURLCopyFileSystemPath(url, kCFURLPOSIXPathStyle);
        CFIndex length = CFStringGetLength(rawwavesPathCFString);
        CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8);
        char *rawwavesPathCString = (char *)malloc(maxSize);
        CFStringGetCString(rawwavesPathCFString, rawwavesPathCString, maxSize, kCFStringEncodingUTF8);
        std::string str(rawwavesPathCString);
        stk::Stk::setRawwavePath(rawwavesPathCString);
        CFRelease(url);
        CFRelease(rawwavesPathCFString);
    }
}


void createMandolin() {

    setRawWavePath();

    myMandolin = new stk::Mandolin(400);
    myMandolin->setFrequency(400);
}

void pluckMandolin() {
    myMandolin->pluck(1);
}

void setMandolinFrequency(float frequency){
    myMandolin->setFrequency(frequency);
}

float nextMandolinSample() {

    float sample = myMandolin->tick();
    
    return sample;
}


















