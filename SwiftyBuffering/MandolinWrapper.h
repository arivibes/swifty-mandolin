//
//  MandoCPP.h
//  SwiftyBuffering
//
//  Created by Ariel Elkin on 06/04/2015.
//  Copyright (c) 2015 Ariel. All rights reserved.
//



#ifdef __cplusplus //C linkage. Exposes the headers to C.
extern "C" {
#endif

    void createMandolin();

    void pluckMandolin();

    float nextMandolinSample();

    void setMandolinFrequency(float frequency);

#ifdef __cplusplus
}
#endif