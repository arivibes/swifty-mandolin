//
//  ViewController.swift
//  SwiftyBuffering
//
//  Created by Ariel Elkin on 06/04/2015.
//  Copyright (c) 2015 Ariel. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var mandolinPlayer = MandolinPlayer()

    @IBAction func tappedPluckMandolin() {
        //Generate mandolin sound samples:
        pluckMandolin()
    }

    @IBAction func changeFrequency(sender:UISlider) {
        setMandolinFrequency(sender.value)
    }
}

class MandolinPlayer{

    var ae:AVAudioEngine
    var player:AVAudioPlayerNode
    var mixer:AVAudioMixerNode
    var buffer:AVAudioPCMBuffer

    init(){

        var sessionError:NSError?
        var success = AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, error: &sessionError)
        if !success {
            println("error setting category: \(sessionError)")
        }
        var hwSampleRate = 44100.0
        success = AVAudioSession.sharedInstance().setPreferredSampleRate(hwSampleRate, error: &sessionError)
        if !success {
            println("error setting sample rate: \(sessionError)")
        }
        var ioBufferDuration:NSTimeInterval = 0.0029
        success = AVAudioSession.sharedInstance().setPreferredIOBufferDuration(ioBufferDuration, error: &sessionError)
        if !success {
            println("error setting sample rate: \(sessionError)")
        }



        // initialize audio engine components:
        ae = AVAudioEngine()
        player = AVAudioPlayerNode()
        mixer = ae.mainMixerNode;


        //Setup AVAudioPCMBuffer:
        var format = AVAudioFormat(standardFormatWithSampleRate: hwSampleRate, channels: 1)

        var frameCapacity = AVAudioFrameCount(44100)

        buffer = AVAudioPCMBuffer(PCMFormat: format, frameCapacity:frameCapacity)

        //Audio samples per buffer:
        buffer.frameLength = AVAudioFrameCount(44100)

        //create mandolin:
        createMandolin()

        // setup audio engine:
        ae.attachNode(player)
        ae.connect(player, to: mixer, format: buffer.format)

        //start audio engine:
        var error: NSError?
        ae.startAndReturnError(&error)
        if (error != nil) {
            println("error starting audio engine: \(error)")
        }

        player.play()

        //start scheduling new buffers
        scheduleNewBuffer()
    }


    func scheduleNewBuffer() {

        //Fill the audio buffers:
        for var i = 0; i < Int(self.buffer.frameLength); i++ {

            var sample = nextMandolinSample()

            self.buffer.floatChannelData.memory[i] = sample
        }

        //schedule another buffer as soon as this one is done playing:
        self.player.scheduleBuffer(self.buffer, atTime: nil, options:.Interrupts, completionHandler: {
            self.scheduleNewBuffer()
        })
    }
}
